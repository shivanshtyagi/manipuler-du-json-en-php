<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/card.min.css">
    
</head>

<body>

<?php

use League\Csv\Reader;

require 'vendor/autoload.php';

$mesDonnees = Reader::createFromPath('cs_figures.csv', 'r');
$mesDonnees->setHeaderOffset(0); 
foreach ($mesDonnees as $valeur) {

?>
<div class="ui card">
  <div class="image">
    <img src="<?php echo $valeur['picture']; ?>">
  </div>
  <div class="content">
    <a class="header"><?php echo $valeur['name']?></a>
    <div class="meta">
      <span class="date"><?php echo $valeur ['birthyear']?></span>
      <br>
      <span class="title"><?php echo $valeur ['title']?></span>
      <br>
      <span class="role"><?php echo $valeur ['role']?></span>
      <br>
      <span class="wikipedia"><?php echo $valeur ['wikipedia']?></span>
      <br>
      <span class="bio"><?php echo $valeur ['bio']?></span>
    </div>
    <div class="description">
    </div>
  </div>
  <div class="extra content">
 </div>
</div>
<?php
}
?>

</body>
</html>